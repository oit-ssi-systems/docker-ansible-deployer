FROM ubuntu:24.04

ENV VAULT_ADDR https://vault-systems.oit.duke.edu
ENV DEBIAN_FRONTEND 'noninteractive'

RUN mkdir /code
COPY . /code/
WORKDIR /code

SHELL ["/bin/bash", "-eo", "pipefail", "-c"]

# hadolint ignore=DL3008
RUN apt-get -y update && \
    apt-get -y install --no-install-recommends \
      curl \
      bzip2 \
      ca-certificates \
      gnupg2 \
      python3-pip \
      python3-hvac \
      git \
      software-properties-common \
      jq \
      rsync \
      tree \
      ansible && \
    curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add - && \
    apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main" && \
    apt-get -y update && \
    apt-get -y install --no-install-recommends \
      vault \
      terraform && \
    setcap -r /usr/bin/vault && \
  rm -rf /var/lib/apt/lists/*
