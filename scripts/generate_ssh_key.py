#!/usr/bin/env python3
import sys
import os
import hvac
import argparse
import subprocess


def parse_args():
    """Parse the arguments."""
    parser = argparse.ArgumentParser(description="Generate an SSH key signed by Vault")
    parser.add_argument(
        "-m",
        "--mount",
        default="ssh-client-signer",
        type=str,
        help="Mount for ssh client signing (Default: ssh-client-signer)",
    )
    parser.add_argument(
        "-g",
        "--gitlab-instance",
        default="gitlab.oit.duke.edu",
        type=str,
        help="Gitlab instance to auth against",
    )
    parser.add_argument(
        "-v", "--verbose", help="Be verbose", action="store_true", dest="verbose"
    )

    parser.add_argument("role", help="SSH Signer Role")

    return parser.parse_args()


def main():

    args = parse_args()

    # Vault connection with local token
    vault = hvac.Client(url=os.environ.get("VAULT_ADDR"))
    if "VAULT_TOKEN" in os.environ:
        sys.stderr.write("Using VAULT_TOKEN auth\n")
        vault.token = os.environ["VAULT_TOKEN"]

    if not os.path.exists("/root/.ssh/id_ed25519"):
        print("Creating ed25519 key")
        subprocess.check_call(
            ["ssh-keygen", "-t", "ed25519", "-N", "", "-f", "/root/.ssh/id_ed25519"]
        )

    with open("/root/.ssh/id_ed25519.pub", "r") as ed25519_pub:
        pub_key = ed25519_pub.read()
        res = vault.write(f"{args.mount}/sign/{args.role}", public_key=pub_key)["data"][
            "signed_key"
        ]
        with open("/root/.ssh/id_ed25519.pub.signed", "w") as ed25519_pub_signed:
            ed25519_pub_signed.write(res)
        os.chmod("/root/.ssh/id_ed25519.pub.signed", 0o600)

    return 0


if __name__ == "__main__":
    sys.exit(main())
