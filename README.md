# Docker Ansible Deployer

Docker image with our standard tools installed.

## What Tools Are Included?

* Ansible (And hvac for vault integrations)

* Vault

* Terraform

* SSH Key Signing Script for Vault

## How do I use the Key signer?

Create a dedicated user that will be allowed to log in from the gitlab runners.
Note that the key you sign will be good for this user on any host that the user
lives on, so you'll want to use something very specific.  For example, if you
are deploying rabbitmq, create a user called `rabbitmq-deploy`.  You should
_not_ use an account this is on hosts that we are not deploying to (think root)

Set up an SSH key signing role in vault.  You can use our helper script from
[duke-vault-helpers](https://git-internal.oit.duke.edu/oit-linux/duke-vault-helpers)
with something like:

```
./scripts/ssh_role_create.py -t $(cat ~/.vault-token) -d rabbitmq-deploy \
  -p rabbitmq-deploy -r rabbitmq-deploy -a rsa-sha2-512
```

More information on client signing is
[here](https://www.vaultproject.io/docs/secrets/ssh/signed-ssh-certificates)

Make sure you have a policy that can use this new role.  Example:

```
path "ssh-client-signer/sign/rabbitmq-deploy" {
  capabilities = ["update"]
}
```

Create a role with the above policy assigned. See
[this](https://gitlab.oit.duke.edu/drews/gitlab-vault-test) for a great way to
create a role that uses native gitlab auth so you don't need to store another
set of credentials!

Finally, get your signed key with a stanza that looks similar to this:

```
  before_script:
    - 'export VAULT_TOKEN="$(vault write -field=token auth/gitlab/login role=rabbitmq jwt=$CI_JOB_JWT)"'
    - '/code/scripts/generate_ssh_key.py rabbitmq-deploy >/dev/null'
```

Note: The first line is getting a token using gitlab authentication against
vault.  The second line uses that token to get the SSH key signed
